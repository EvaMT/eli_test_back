import express, {Router} from "express";
import {Controller} from "../controller/controller";

const router = Router();
const controller: Controller = new Controller();

router.get("/", async (req: express.Request, res: express.Response) => controller.get(req, res));

router.post("/actions", async (req: express.Request, res: express.Response) => controller.postActions(req, res));

export default router;
