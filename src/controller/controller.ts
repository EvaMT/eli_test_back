import express from "express";

export class Controller {
    constructor() {}

    async get(req: express.Request, res: express.Response) {
        res.send("Yo, that's a POST endpoint! You don't GET it... lmao");
    }

    async postActions(req: express.Request, res: express.Response) {
        res.send("Post /actions is not implemented yet... Work in progress tho");
    }
}
