import express from "express";
import router from "./routes/routes";
import * as bodyParser from "body-parser";

import dotenv from "dotenv";
dotenv.config();

const port = 3000;
const app = express();

app.use(loggerMiddleware, bodyParser.json(), router);

function loggerMiddleware(
    req: express.Request,
    res: express.Response,
    next: any
) {
    console.log(`${req.method} ${req.path}`);
    next();
}

app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(
        `Server started at http://localhost:${port} in ${process.env.NODE_ENV} environment`
    );
});

